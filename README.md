
<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->
<a href="http://bit.ly/LinkedIn-Andre"><img src="https://img.shields.io/badge/linkedin-blue.svg?&style=for-the-badge&logo=linkedin&logoColor=white" height=25></a> 



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="http://bit.ly/GitHub-Andre" target=“_blank”>
    <img src="https://user-images.githubusercontent.com/48434290/121789356-356cf780-cbcd-11eb-89f4-ae61780c29b8.png" width="550px" alt="Logo Photo Bomb">
  </a>

  <h3 align="center">Cube Dash!</h3>

  <p align="center">
    O Cube Dash....
    <br />
    <br />
    <a href="https://github.com/andrenevesgomes/Cube-Dash" target=“_blank”><strong>Explorar os docs »</strong></a>
    <br />
    <br />
    <a href="#" target=“_blank”>Ver Demo</a>
    ·
    <a href="https://github.com/andrenevesgomes/Cube-Dash/issues" target=“_blank”>Reportar Bug</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
# Tabela de Conteúdos

* [Sobre o Projeto](#sobre-o-projeto)
  * [Desafio](desafio)
  * [Solução](#solução)
  * [Bibliotecas Usadas](#bibliotecas-usadas)
* [Início](#início)
  * [Pré-requisitos](#pré-requisitos)
* [Uso](#uso)
* [Licença](#licença)
* [Colaboradores](#colaboradores)



<!-- SOBRE O PROJETO -->
# Sobre o Projeto

![Menu](https://user-images.githubusercontent.com/48434290/117454928-8d656e00-af3e-11eb-9396-2a7a0392e4a4.png)

O Cube Dash......


## Desafio
Efectuar um jogo para a disciplina de técnica de simulação do professor José Neves de forma a demonstrar os nossos conhecimentos adquiridos ao longo do semestre.


## Solução
.....


## Bibliotecas Usadas
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Pretty checkbox](https://lokesh-coder.github.io/pretty-checkbox/)
* [Popper.js](https://popper.js.org/)
* [Font Awesome](https://fontawesome.com)



<!-- GETTING STARTED -->
# Início

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

## Pré-requisitos

This is an example of how to list things you need to use the software and how to install them.
* npm
```sh
npm install npm@latest -g
```


<!-- USAGE EXAMPLES -->
# Uso

1. Get a free API Key at [https://example.com](https://example.com)
2. Clone the repo
```sh
git clone https://github.com/your_username_/Project-Name.git
```
3. Install NPM packages
```sh
npm install
```
4. Enter your API in `config.js`
```JS
const API_KEY = 'ENTER YOUR API';
```


<!-- LICENSE -->
# Licença

Check the license right [here!](https://github.com/andrenevesgomes/Cube-Dash/blob/master/LICENSE.md)



<!-- ALL-CONTRIBUTORS-LIST -->
# Colaboradores

<table>
  <tr>
    <td align="center"><a href="https://github.com/andrenevesgomes"><img src="https://user-images.githubusercontent.com/48434290/95869306-23e3ff00-0d63-11eb-89ea-fa1e8a6b95ae.jpg" width="100px;" alt=""/><br /><sub><b>André Gomes</b></sub></a></br><a href="https://andrenevesgomes.github.io/portfolio/" title="Portfólio">:book:</a></td></tr>
</table>

